# Copyright 2020, 2021 freedesktop-sdk
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# This file is a modified derivative of the Collect Manifest plugin,
# released under the same license
#
# Authors:
#        Valentin David <valentin.david@gmail.com> (Collect Manifest)
#        Adam Jones <adam.jones@codethink.co.uk>   (Collect Manifest)
#        Douglas Winship <douglas.winship@codethink.co.uk> (Url Manifest)
#        Abderrahim Kitouni <akitouni@gnome.org> (Buildstream plugin -> script)

# A script used to produce a manifest file containing a list of source urls for
# a given dependency, and all its subdependencies, down to the bottom of the
# tree.
#
# The manifest contains information such as:
#     - the alias used in the url (null if no alias is used)
#     - the 'main' source url
#
# The manifest file is exported as a json file to the path provided
# under the "path" variable defined in the .bst file.
#
# We'd also like to include the list of mirror urls (which may be empty).

import argparse
import dataclasses
import glob
import operator
import os
import posixpath  # useful for url paths
import re
import sys
from argparse import RawTextHelpFormatter
from collections import namedtuple

import yaml
from buildstream._frontend.app import App
from buildstream.types import _PipelineSelection


def simplify_glob(track):
    "Replace a glob expression with a simpler one that only contains (at most) one asterisk"
    track = track.replace("?", "*")
    track = re.sub(r"\[[^\]]+\]", "*", track)
    if track.count("*") > 1:
        # drop everything between the first and last asterisk
        track = track[: track.index("*")] + track[track.rindex("*") :]
    return track


def extract_alias(source, raw_url):
    translated_url = source.translate_url(raw_url, primary=False)
    if raw_url == translated_url:
        return None, None, translated_url

    alias, rest = raw_url.split(":", 1)
    return alias, translated_url.removesuffix(rest), translated_url


def gen_file_entry(source, raw_url, *, suffix="", sha256sum=None):
    alias, alias_url, url = extract_alias(source, raw_url)
    if suffix:
        url = posixpath.join(url, suffix)

    return {
        "type": "file",
        "alias": alias,
        "alias_url": alias_url,
        "url": url,
        "sha256sum": sha256sum,
    }


def gen_git_entry(args, source, raw_url, track=None, ignore_patterns=None):
    alias, alias_url, translated_url = extract_alias(source, raw_url)

    refspecs = []
    ref_patterns = []

    if track:
        # grab a copy of track before it is overriden by simplify_glob
        ref_patterns.append(track)
        track = simplify_glob(track)
        if track.startswith("refs/") or "*" not in track:
            refspecs = [track]
        else:
            refspecs = [f"refs/heads/{track}", f"refs/tags/{track}"]
    if refspecs and args.git_default_branches:
        refspecs.append("refs/heads/ma*")

    return dict(
        type="git",
        alias=alias,
        alias_url=alias_url,
        url=translated_url,
        ref_patterns=ref_patterns,
        ignore_patterns=ignore_patterns if ignore_patterns else [],
        refspecs=refspecs,
    )


def get_source_locations(args, sources):
    """
    Returns a list of source URLs and refs, currently for
    git, tar, ostree, remote, zip and tar sources.
    Patch sources are not included in the output, since
    they don't have source URLs

    :args Program arguments from argparse
    :sources A list or generator of BuildStream Sources
    """
    source_locations = []
    for source in sources:
        source_kind = source.get_kind()
        source_ref = source.get_ref()
        if source_kind in ("local", "patch", "patch_queue", "live_bootstrap_prepare"):
            # Path inside the project
            continue
        elif source_kind == "cargo":
            for crate in source.get_source_fetchers():
                path = "{name}/{name}-{version}.crate".format(
                    name=crate.name, version=crate.version
                )
                source_locations.append(
                    gen_file_entry(source, source.url, suffix=path, sha256sum=crate.sha)
                )
        elif source_kind == "cargo2":
            for crate in source.get_source_fetchers():
                crate_kind = crate.ref_node().get("kind", "registry")
                if crate_kind == "registry":
                    path = "{name}/{name}-{version}.crate".format(
                        name=crate.name, version=crate.version
                    )
                    source_locations.append(
                        gen_file_entry(
                            source, source.url, suffix=path, sha256sum=crate.sha
                        )
                    )
        elif source_kind in ("tar", "zip", "remote"):
            source_locations.append(
                gen_file_entry(source, source.original_url, sha256sum=source_ref)
            )
        elif source_kind == "pypi":
            source_locations.append(
                gen_file_entry(
                    source,
                    source.base_url,
                    suffix=source.fetcher.suffix,
                    sha256sum=source_ref.get("sha256sum"),
                )
            )
        elif source_kind == "cpan":
            source_locations.append(
                gen_file_entry(
                    source,
                    source.orig_index,
                    suffix=source.fetcher.suffix,
                    sha256sum=source_ref.get("sha256sum"),
                )
            )
        elif source_kind == "live_bootstrap_manifest":
            for entry in source.get_source_fetchers():
                source_locations.append(
                    gen_file_entry(source, entry.url, sha256sum=entry.sha256sum)
                )
        elif source_kind == "git_repo":
            ignore_patterns = list(source.exclude)
            source_locations.append(
                gen_git_entry(
                    args, source, source.url, source.tracking, ignore_patterns
                )
            )
        elif source_kind in ("git", "git_tag"):
            for num, mirror in enumerate(source.get_source_fetchers()):
                # Only fetch for the first item contains the refspec/ref_patterns. The remaining sources
                # are submodules and we must not share tracking with them as it will generate invalid
                # lorry entries
                if num == 0:
                    source_locations.append(
                        gen_git_entry(args, source, mirror.url, source.tracking)
                    )
                else:
                    source_locations.append(gen_git_entry(args, source, mirror.url))
        elif source_kind in ("git_module", "go_module"):
            for mirror in source.get_source_fetchers():
                source_locations.append(gen_git_entry(args, source, mirror.url))
        elif source_kind == "docker":
            # Ignore for now
            pass
        else:
            sys.exit(f"ERROR: unhandled source plugin {source_kind}")

    return source_locations


GitLorry = namedtuple(
    "GitLorry",
    (
        "name",
        "url",
        "check_certificates",
        "lfs",
        "gc",
        "refspecs",
        "ref_patterns",
        "ignore_patterns",
    ),
)


@dataclasses.dataclass
class RawFileEntry:
    url: str
    destination: str | None = None
    sha256sum: str | None = None

    def alias_key(self):
        """Return a tuple of the attributes that make a lorry entry unique

        sha256sum is ignored here to ensure existing entries are updated when a
        sha256sum is supplied.
        """
        if not self.destination:
            return (self.url,)
        return (self.url, self.destination)


# Write git lorry files
def process_git(source_url, alias_url):
    path = source_url.removeprefix(alias_url)
    return path.removesuffix(".git")


# Write raw-file lorry files
def process_raw_file(source_url, alias_url):
    path = source_url.removeprefix(alias_url)
    return os.path.dirname(path)


# Write lorry file
def write_file(alias, entries, kind, file_path, refspec_format):
    with open(file_path, "w", encoding="utf-8") as file:
        if kind == "git":
            data = {}
            for lorry in sorted(entries):
                git_entry = {"type": "git", "url": lorry.url}
                if lorry.check_certificates is not None:
                    git_entry["check-certificates"] = lorry.check_certificates
                if lorry.lfs is not None:
                    git_entry["lfs"] = lorry.lfs
                if lorry.gc is not None:
                    git_entry["gc"] = lorry.gc
                if refspec_format:
                    if lorry.refspecs:
                        git_entry["refspecs"] = list(lorry.refspecs)
                else:
                    if lorry.ref_patterns:
                        git_entry["ref_patterns"] = list(lorry.ref_patterns)
                    if lorry.ignore_patterns:
                        git_entry["ignore_patterns"] = list(lorry.ignore_patterns)

                data[f"{alias}/{lorry.name}"] = git_entry
            yaml.dump(data, file, sort_keys=False)
        elif kind == "raw-file":
            urls = []
            for entry in sorted(entries, key=operator.attrgetter("url")):
                item = {"url": entry.url}
                if entry.destination:
                    item["destination"] = entry.destination
                if entry.sha256sum:
                    item["sha256sum"] = entry.sha256sum
                urls.append(item)
            data = {f"{alias}/raw-files": {"type": "raw-file", "urls": urls}}
            yaml.dump(data, file, sort_keys=False)


def extract_manifest(
    data,
    git_dir,
    raw_files_dir,
    refs_mode,
    exclude_aliases,
    namespace_projects,
    refspec_format,
):
    aliases = set()

    # Dicts to hold results
    git_result = {}
    raw_file_result = {}

    glob_pattern = "*/*.lorry" if namespace_projects else "*.lorry"
    lorry_files = [
        (f.removesuffix(".lorry"), os.path.join(git_dir, f))
        for f in glob.glob(glob_pattern, root_dir=git_dir)
    ]
    lorry_files += [
        (f.removesuffix(".lorry"), os.path.join(raw_files_dir, f))
        for f in glob.glob(glob_pattern, root_dir=raw_files_dir)
    ]

    # Load lorry files
    for alias, filepath in lorry_files:
        with open(filepath, "r", encoding="utf-8") as file:
            content = yaml.safe_load(file)
        aliases.add(alias)

        for k, v in content.items():
            if v["type"] == "git":
                name = k.removeprefix(alias + "/")
                url = v["url"]
                refspecs = tuple(v.get("refspecs", []))
                ref_patterns = tuple(v.get("ref_patterns", []))
                ignore_patterns = tuple(v.get("ignore_patterns", []))
                check_certificates = v.get("check-certificates", None)
                lfs = v.get("lfs", None)
                gc = v.get("gc", None)
                alias_result = git_result.setdefault(alias, {})
                alias_result[url] = GitLorry(
                    name,
                    url,
                    check_certificates,
                    lfs,
                    gc,
                    refspecs,
                    ref_patterns,
                    ignore_patterns,
                )

            elif v["type"] == "raw-file":
                alias_result = raw_file_result.setdefault(alias, {})
                for entry in v["urls"]:
                    raw_file_entry = RawFileEntry(**entry)
                    alias_result[raw_file_entry.alias_key()] = raw_file_entry

    # Loop through each entry within manifest.json
    for entry in data:
        for source in entry["sources"]:
            typ = source["type"]
            url = source["url"]
            alias = source["alias"]
            alias_url = source["alias_url"]

            if alias_url is None:
                continue

            aliases.add(alias)

            if typ == "git":
                alias_result = git_result.setdefault(alias, {})
                git_entry = alias_result.get(url)
                if refs_mode == "remove":
                    # exclude all refs from the config, to enable whole repo mirroring for existing and new repos.
                    ignore_patterns = set()
                    ref_patterns = set()
                    refspecs = set()
                elif refs_mode == "ignore":
                    # Keep existing refs config but don't add new ones, to enable existing repos to keep existing refspec while new repos mirror the whole repo.
                    if git_entry:
                        refspecs = set(git_entry.refspecs)
                        ref_patterns = set(git_entry.ref_patterns)
                        ignore_patterns = set(git_entry.ignore_patterns)
                    else:
                        refspecs = set()
                        ref_patterns = set()
                        ignore_patterns = set()
                elif refs_mode == "replace":
                    # Replace old refs with new ones
                    refspecs = set(source["refspecs"])
                    ref_patterns = set(source["ref_patterns"])
                    ignore_patterns = set(source["ignore_patterns"])
                else:
                    # Default case: refs_mode == "update"
                    # Keep existing refs in addition to adding new entries, to enable specific mirroring based on refspec.
                    refspecs = set(source["refspecs"])
                    ref_patterns = set(source["ref_patterns"])
                    ignore_patterns = set(source["ignore_patterns"])
                    if git_entry:
                        refspecs.update(git_entry.refspecs)
                        ref_patterns.update(git_entry.ref_patterns)
                        ignore_patterns.update(git_entry.ignore_patterns)

                if refspecs == {"refs/heads/*", "refs/tags/*"}:
                    refspecs = set()

                name = process_git(url, alias_url)
                check_certificates = git_entry.check_certificates if git_entry else None
                lfs = git_entry.lfs if git_entry else None
                gc = git_entry.gc if git_entry else None

                alias_result[url] = GitLorry(
                    name,
                    url,
                    check_certificates,
                    lfs,
                    gc,
                    sorted(refspecs),
                    sorted(ref_patterns),
                    sorted(ignore_patterns),
                )

            elif typ in "file":
                alias_result = raw_file_result.setdefault(alias, {})
                raw_file_entry = RawFileEntry(
                    url=url,
                    destination=process_raw_file(url, alias_url),
                    sha256sum=source["sha256sum"],
                )
                alias_result[raw_file_entry.alias_key()] = raw_file_entry

            else:
                print(f"WARNING: unhandled mirror type {typ}")

    # Write lorry file
    for alias in aliases:
        if alias in exclude_aliases:
            print(f"WARNING: skipping excluded alias: {alias}")
            continue
        if alias in git_result and alias in raw_file_result:
            sys.exit(
                f"ERROR: Alias {alias} is used for both git repositories and raw files"
            )

        if alias in git_result:
            filepath = os.path.join(git_dir, f"{alias}.lorry")
            write_file(
                alias, git_result[alias].values(), "git", filepath, refspec_format
            )

        if alias in raw_file_result:
            filepath = os.path.join(raw_files_dir, f"{alias}.lorry")
            write_file(
                alias, raw_file_result[alias].values(), "raw-file", filepath, False
            )


def main():
    parser = argparse.ArgumentParser(
        description="Write Lorry files based on bst requirements",
        formatter_class=RawTextHelpFormatter,
    )
    parser.add_argument(
        "elements", metavar="element", nargs="+", help="Element files to process"
    )
    parser.add_argument(
        "--git-directory",
        dest="git_dir",
        default=".",
        help="Directory to write git lorry files to",
    )
    parser.add_argument(
        "--raw-files-directory",
        dest="raw_files_dir",
        default=".",
        help="Directory to write raw-files lorry files to",
    )
    parser.add_argument(
        "--refs-mode",
        dest="refs_mode",
        default="update",
        choices=["update", "replace", "remove", "ignore"],
        help="""The ref mode to use:
- update: Add new refs to existing refs (All repositories will be set to mirror new refs + the old refs)
- replace: Replace existing refs with new refs (All repositories will be set to mirror new refs ignoring old)
- remove: Remove all refs (All repositories will be set to mirror the entire repository)
- ignore: Keep existing refs, and don't add new refs (Newly added repositories will default to the entire repository)

WARNING: Choose carefully which refs mode you actually want and need. When tracking branches, force pushes to these branches will break the mirroring. So in active projects tracking dev branches will routinely stop the mirroring.
""",
    )
    parser.add_argument(
        "--exclude-alias",
        dest="exclude_aliases",
        action="append",
        default=[],
        help="Alias to exclude from the mirroring config",
    )
    parser.add_argument(
        "--namespace-projects",
        dest="namespace_projects",
        action="store_true",
        help="Use a separate namespace for the mirrors of each project",
    )
    parser.add_argument(
        "--git-default-branches",
        dest="git_default_branches",
        action="store_true",
        help="Force default branches ma* into refspec for git",
    )
    parser.add_argument(
        "-o",
        "--option",
        dest="options",
        default=[],
        nargs=2,
        action="append",
        help="Options to pass to buildstream",
    )
    parser.add_argument(
        "--refspecs",
        dest="refspecs",
        default=False,
        action="store_true",
        help="Write file with refspecs instead of lorry2 ref_patterns and ignore_patterns",
    )
    args = parser.parse_args()

    # Ensure output directory exists
    for directory in [args.git_dir, args.raw_files_dir]:
        os.makedirs(directory, exist_ok=True)

    manifest = []
    visited_names_list = set()

    app = App.create({
        "no_interactive": True,
        "colors": True,
        "directory": "",
        "config": "",
        "log_file": "",
        "option": args.options,
    })

    with app.initialized():
        for dep in app.stream.load_selection(
            args.elements, selection=_PipelineSelection.ALL
        ):
            if dep._get_full_name() in visited_names_list:
                continue
            visited_names_list.add(dep._get_full_name())

            sources = get_source_locations(args, dep.sources())
            if sources:
                if args.namespace_projects:
                    for source in sources:
                        source["alias"] = f"{dep.project_name}/{source['alias']}"
                    # Ensure output namespace project directory exists
                    for directory in [
                        os.path.join(args.git_dir, dep.project_name),
                        os.path.join(args.raw_files_dir, dep.project_name),
                    ]:
                        os.makedirs(directory, exist_ok=True)
                manifest.append({"element": dep.name, "sources": sources})

    extract_manifest(
        manifest,
        args.git_dir,
        args.raw_files_dir,
        args.refs_mode,
        args.exclude_aliases,
        args.namespace_projects,
        args.refspecs,
    )


if __name__ == "__main__":
    main()
